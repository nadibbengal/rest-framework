from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    UpdateAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveDestroyAPIView,
    RetrieveUpdateAPIView

)
from rest_framework.response import Response
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,

)
from .pagination import questionpagenumberpagination
from rest_framework.filters import(
    SearchFilter,
    OrderingFilter
)

from .permissions import isowner
from .serializers import (
    PollDetailSerializers,
    PollListSerializers,
    PollCreateSerializers,
    InterestListSerializers,)

from .decorators import timer
from polls.models import Question,Choice,VoteViewCounter

from profileinfo.models import interest

class QuestionCreateAPIView(CreateAPIView):
    
    queryset=Question.objects.all()
    serializer_class=PollCreateSerializers
    permission_classes=[IsAuthenticated]

    @timer
    def post(self, request):
        tag=interest.objects.get(id=request.data['tag'])
        serializer = PollCreateSerializers(data=request.data)
        if serializer.is_valid(raise_exception=True):
            q = serializer.save(user=request.user)
            options=request.POST.getlist('options[]')
            for option in options:
                opt_obj=Choice(question=q,choice_text=option)
                opt_obj.save()
            counter=VoteViewCounter(Question=q)
            counter.save()
            counter.votedUser.add(request.user)
            counter.save()
            counter.viewedUser.add(request.user)
            counter.save()
        return Response(serializer.data)
            

    


class QuestionlistAPIView(ListAPIView):
    serializer_class=PollListSerializers
    filter_backends=[SearchFilter]
    search_fields = ['user__username','tag__title']
    pagination_class = questionpagenumberpagination

    def get_queryset(self,*args,**kwargs):
        query=Question.objects.order_by('-pub_date')
        return query

    

class QuestionDetailAPIView(RetrieveAPIView):
    queryset=Question.objects.all()
    serializer_class=PollDetailSerializers
    lookup_field='id'


class QuestionUpdateAPIView(RetrieveUpdateAPIView):
    queryset=Question.objects.all()
    serializer_class=PollDetailSerializers
    lookup_field='id'
    permission_classes=[IsAuthenticatedOrReadOnly,isowner]

class QuestionDeleteAPIView(RetrieveDestroyAPIView):
    queryset=Question.objects.all()
    serializer_class=PollDetailSerializers
    lookup_field='id'
    
    def get(self,request,id):
        question=Question.objects.get(id=id)
        question.delete()
        return Response("deleted") 
        

class InterestListAPIView(ListAPIView):
    serializer_class=InterestListSerializers
    queryset=interest.objects.all()

class ChoiceCreateAPIview(CreateAPIView):
    
    queryset=Choice.objects.all()
    serializer_class=PollCreateSerializers
    permission_classes=[IsAuthenticated,IsAdminUser]

    def perform_create(self,serializer):
        serializer.save(user=self.request.user)
   