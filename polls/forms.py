from django import forms
from . import models

class createpoll(forms.ModelForm):
    class Meta:
        model=models.Question
        fields=['question_text','tag']
  
    def clean_question_text(self):
        data = self.cleaned_data['question_text']  
        if "?" not in data:
            raise forms.ValidationError("Not a question")
        return data


class takechoice(forms.ModelForm):
    class Meta:
        model=models.Choice
        fields=['choice_text']
    def getit(self):
        return models.Choice.choice_text
