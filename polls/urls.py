from django.conf.urls import url
from . import views
app_name='polls'
urlpatterns = [
    url(r'^$',views.index,name='index'),
    url(r'^profile$',views.profile,name='profile'),
    url(r'^test$',views.test,name='test'),
    url(r'^admin$',views.admin,name='admin'),
    url(r'^createpolls$',views.createpolls,name='createpoll'),
    # ex: /polls/5/
    url(r'^(?P<question_id>[0-9]+)/$', views.detail, name='detail'),
    # ex: /polls/5/results/
    url(r'^(?P<question_id>[0-9]+)/results/$', views.results, name='results'),
    # ex: /polls/5/vote/
    url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
    url(r'^(?P<question_id>[0-9]+)/getdata/$', views.getdata, name='getdata'),
    
]