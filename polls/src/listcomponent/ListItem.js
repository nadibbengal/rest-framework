import React, { Component } from 'react'

export default class ListItem extends Component {
  render() {   
        var {questions}=this.props
        if(questions){
           return(
            questions.map(question =>
            {
                var link ="http://localhost:8000/poll/"+ question.id + "/" ;
                const style={textDecoration: 'none',}
                return (
                <div key={question.id}>
               <div className="card">
                <a href={link} style={style}>
                <div className="card-body"> {question.question_text} </div>
                <div className="card-footer">Auther: {question.user}  Tag: {question.tag} </div>
                </a>
                </div>
                <br/>
                </div>
            )
            })
        )
        }
        else
        {
            return <div></div>
        }
  }
}
