import React,{Component} from 'react'
import Listitem from './ListItem'
import Pagemanager from './Pagemanager'
class ListApp extends Component{

    state={
        data:null
    }
    getdata =(link)=>{
        fetch(link).
        then(result =>{
           return result.json();
        }).then(data =>{
            //console.log(data)
            this.setState(
                {
                    data:data
                }
            )
        })
    }
    componentDidMount(){
        
        this.getdata('http://localhost:8000/api/poll/')
    }
    
    render(){
        var {data}=this.state;
        console.log(data)
        const style={
            textAlign:'auto'
        }
        if(data){
            return (
                <div>
                <Listitem questions={data.results}></Listitem>
                <div style={style}>
                <Pagemanager getdata={this.getdata} count={data.count} next={data.next} previous={data.previous}></Pagemanager>
                </div>
                </div>
             )
        }
        else{
            return (
                <div className="spinner-grow" role="status">
                <span className="sr-only">Loading...</span>
                </div>
            )
        }
        
    }
}

export default ListApp;
