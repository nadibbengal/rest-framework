import React,{Component} from 'react'
import Tabledata from './Tabledata'
import Graphicaldata from './Graphicaldata'
import {HashRouter,Switch,Route,Link} from 'react-router-dom'
class App extends Component{

    
    render(){
        const style={margin:'15px',}
        console.log(this.props)
        return (
        <HashRouter>
        <Link to="/" className="btn btn-info" style={style} role="button">Tablurar Format</Link>
        <Link to="/graphicaldata" className="btn btn-info" role="button">Graphical Format</Link>
        <div className="container">
        <Switch>
        <Route exact path="/" component={Tabledata}/>
        <Route  path="/graphicaldata" component={Graphicaldata}/>
        </Switch>
        </div>
        </HashRouter>
    )
    }
}

export default App;
