import React, { Component } from 'react'
import {Link} from 'react-router-dom'
export default class Tabledata extends Component {
    state={
        data:null
    }
    componentDidMount()
    {   
        var id=window.location.pathname.slice(6);
        fetch('http://localhost:8000/api/poll/'+id).
        then(result =>{
           return result.json();
        }).then(data =>{
            console.log(data)
            this.setState(
                {
                    data:data
                }
            )
        })
    }
    render() {
        const {data}=this.state;
        if(data){
            return (
            
            <div>
            <table className="table table-hover">
                <thead>
                    <tr>
                    <th>Cetagory</th>
                    <th>Number of Votes</th>
                    </tr>
                </thead>
                <tbody>
                    {data.choices && data.choices.map(choice =>{
                return (
                    <tr key={choice.id}>
                        <td>{ choice.choice_text }</td>
                        <td>{ choice.votes } votes</td>
                    </tr> 
                    )
                })
                }                   
            </tbody>
            </table>
            
            </div>
            )
        }
        else
        {
            return (
                <div className="spinner-border" role="status">
                <span className="sr-only">Loading...</span>
                </div>
            )
        }
       
    }
}
