import React, { Component } from 'react'
import {Link} from 'react-router-dom'
export default class Getdata extends Component {
    state={
        data:null
    }
    componentDidMount()
    {
        fetch('http://localhost:8000/api/poll/').
        then(result =>{
           return result.json();
        }).then(data =>{
            this.setState(
                {
                    data:data
                }
            )
        })
    }
    render() {
        const {data}=this.state;
        if(data){
           
            const questions=data.results
            return (
            <div>
            <h1>hello</h1>
            {questions && questions.map(question =>{
               return (
                <div key={question.id}  className="card" >
                <h3>{question.question_text}</h3>
                <Link to="/second">Click Here</Link> to contact us!
                <br/>
                </div>
                )
            })
            }
            </div>
            )
        }
        else
        {
            return (
                <div className="spinner-border" role="status">
                <span className="sr-only">Loading...</span>
                </div>
            )
        }
       
    }
}
