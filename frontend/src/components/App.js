import React,{Component} from 'react'
import Getdata from './Getdata'
import Second from './Second'
import {HashRouter,Switch,Route} from 'react-router-dom'
class App extends Component{

    
    render(){
        console.log(this.props)
        return (
        <HashRouter>
        <div className="App">
        <Switch>
        <Route exact path="/" component={Getdata}/>
        <Route exact path="/second" component={Second}/>
        </Switch>
        </div>
        </HashRouter>
    )
    }
}

export default App;
