# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-03-23 12:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profileinfo', '0002_interest'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='info',
            name='interest',
        ),
        migrations.AddField(
            model_name='info',
            name='interest',
            field=models.ManyToManyField(to='profileinfo.interest'),
        ),
    ]
