# The Voting App
---

## Instalation

You’ll have to run few python and npm scripts before using this app.

1. First  **Clone** this Repository.
2. Then go to the directory where you cloned.
3. Write **npm run dev-polls** then **npm run dev-accounts**. 
4. Write **python manage.py runserver** To start the server.
5. Then  **enjoy** online voting app 

---

# Technology Used for this project.

1. **Django**
2. **Django Rest Api**.
3. **React.js**
4. **Bootstrap 4**


---
